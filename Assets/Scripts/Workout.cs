﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Workout : MonoBehaviour
{
    public int workoutID;
    public int userID;
    public string workoutName;
    public int totalExcsNumber;
    public bool isCompleted;
    public Exercise[] exercises;
}

