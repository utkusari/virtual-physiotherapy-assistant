﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginSystem : MonoBehaviour
{
    public InputField email;
    public InputField password;

    public Button loginButton;

    public void CallLogin()
    {
        StartCoroutine(Login());
    }

    IEnumerator Login()
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email.text);
        form.AddField("password", password.text);

        WWW www = new WWW("http://localhost:81/sqlconnect/login.php", form);
        yield return www;
        if (www.text[0] == '0')
        {
            DBManager.email = email.text;
            DBManager.name = www.text.Split('\t')[1];
            DBManager.surname = www.text.Split('\t')[2];
            Debug.Log(DBManager.name + " " + DBManager.surname + " kullanicisi giris yapti");
            SceneManager.LoadScene(3);
        }
        else
        {
            Debug.Log("User login failed. Error: " + www.text);
        }
    }

    public void VerifyInput()
    {
        loginButton.interactable = (email.text.Length >= 10 && password.text.Length >= 6);
    }
}
