﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomePageUISystem : MonoBehaviour
{

    public Text homePageLabel;

    // Start is called before the first frame update
    void Start()
    {
        homePageLabel.text = DBManager.name + " " + DBManager.surname + "'s Home Page";
    }

}
