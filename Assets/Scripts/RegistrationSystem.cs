﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RegistrationSystem : MonoBehaviour
{
    public InputField email;
    public InputField name;
    public InputField surname;
    public InputField password;

    public Button registerButton;

    public void CallRegister()
    {
        StartCoroutine(Register());
    }

    IEnumerator Register()
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email.text);
        form.AddField("name", name.text);
        form.AddField("surname", surname.text);
        form.AddField("password", password.text);

        WWW www = new WWW("http://localhost:81/sqlconnect/register.php", form);   
        yield return www;
        if (www.text == "0")
        {
            Debug.Log("Registered");
            SceneManager.LoadScene(0);
        }
        else
        {
            Debug.Log("Failed. Error: " + www.text);
        }

    }

    public void VerifyInput()
    {
        registerButton.interactable = (email.text.Length >= 10 && name.text.Length >= 3 && surname.text.Length >= 2 && password.text.Length >= 6);
    }
        
}
