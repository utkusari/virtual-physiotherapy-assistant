﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
    
public class WorkoutManager : MonoBehaviour
{
    public Workout workout;
    public Button goToWorkoutButton;
    public Text buttonText;
    public Button resetButton;

    public void isCompleted()
    {
        int falseCount = 0;
        foreach (Exercise ex in workout.exercises)
        {
            if (!ex.isCompleted)
            {
                falseCount++;
            }
        }

        if (falseCount == 0)
        {
            goToWorkoutButton.interactable = false;
            buttonText.text = "Workout Completed";
        }
    }

    public void ResetWorkouts()
    {
        PlayerPrefs.DeleteAll();
    }

    void Update()
    {
        isCompleted();
    }
}
