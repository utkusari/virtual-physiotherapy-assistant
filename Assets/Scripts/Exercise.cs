﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Exercise : MonoBehaviour
{
    public int exID;
    public int woID;
    public string exName;
    public bool isCompleted;
    public float successRate;

    public Toggle checkBox;
    public Text successRateText;

    public Exercise(int id, int woid, string name)
    {
        exID = id;
        woID = woid;
        exName = name;
        isCompleted = false;
        successRate = 0.0f;
    }

    public void isDone()
    {
        if (PlayerPrefs.GetInt("SquatComp") == 1 && gameObject.tag == "SQUAT")
        {
            isCompleted = true;
            successRate = PlayerPrefs.GetFloat("SquatRate");
            checkBox.isOn = true;
            successRateText.text = successRate.ToString();
            
        }
        else if (PlayerPrefs.GetInt("PushComp") == 1 && gameObject.tag == "PUSH")
        {
            isCompleted = true;
            successRate = PlayerPrefs.GetFloat("PushRate");
            checkBox.isOn = true;
            successRateText.text = successRate.ToString();
            
        }
        else if (PlayerPrefs.GetInt("RTEComp") == 1 && gameObject.tag == "RTE")
        {
            isCompleted = true;
            successRate = PlayerPrefs.GetFloat("RTERate");
            checkBox.isOn = true;
            successRateText.text = successRate.ToString();
            
        }
        else if (PlayerPrefs.GetInt("LTEComp") == 1 && gameObject.tag == "LTE")
        {
            isCompleted = true;
            successRate = PlayerPrefs.GetFloat("LTERate");
            checkBox.isOn = true;
            successRateText.text = successRate.ToString();
            

        }
        else if (PlayerPrefs.GetInt("LAUComp") == 1 && gameObject.tag == "LAU")
        {
            isCompleted = true;
            successRate = PlayerPrefs.GetFloat("LAURate");
            checkBox.isOn = true;
            successRateText.text = successRate.ToString();
            
        }
        
    }

    private void LateUpdate()
    {
        isDone();
    }
}
